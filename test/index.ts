import { ethers } from "hardhat";

export const deployContract = async () => {
  const Glootie = await ethers.getContractFactory("Glootie");
  const glootie = await Glootie.deploy();
  await glootie.deployed();
  return glootie;
};

describe("Glootie NFT...", function () {
  const DEPLOYER = "0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266";
  const RECIPIENT = "0x70997970C51812dc3A010C7d01b50e0d17dc79C8";

  const TOKEN_ID = 1;
  const URI = "QmaHubThdh7UQS41ou9LZXBJ9CQNf1XbUKrLRTJhMkecVL";

  it("should mint an NFT", async function () {
    const glootie = await deployContract();
    // Mint NFT
    const nft = await glootie.safeMint(DEPLOYER, TOKEN_ID, URI);
    // wait until the transaction is mined
    await nft.wait();

    // Contract Name
    const name = await glootie.name();
    // Contract Name
    const symbol = await glootie.symbol();
    // Get the NFT URI
    const tokenURI = await glootie.tokenURI(TOKEN_ID);
    // Token Owner of ID
    const tokenOwner = await glootie.ownerOf(TOKEN_ID);

    console.log(`Owner of Mint #${TOKEN_ID}:`, tokenOwner);
    console.log("Name: ", name);
    console.log("Symbol: ", symbol);
    console.log("Location: ", tokenURI);
  });

  it("can be transferred", async function () {
    const glootie = await deployContract();
    // Mint NFT
    const nft = await glootie.safeMint(DEPLOYER, TOKEN_ID, URI);
    // wait until the transaction is mined
    await nft.wait();

    const initialOwner = await glootie.ownerOf(TOKEN_ID);
    console.log("Initial Owner: ", initialOwner);

    await glootie.transferFrom(initialOwner, RECIPIENT, TOKEN_ID);
    console.log("transferring NFT...");

    const newOwner = await glootie.ownerOf(TOKEN_ID);
    console.log("New Owner: ", newOwner);
  });
});
